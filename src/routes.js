import login from './components/LoginScreen.vue';
import Students from './components/students/Students.vue'
import Subjects from './components/subjects/Subjects.vue'
import Dashboard from './components/Dashboard.vue'
// import test from './components/testAPI.vue'

export default [
  {path: '/', component:login, name: 'Login'},
  // {path: '/test', component:test},
  {path: '/dashboard', component: Dashboard,
    children: [
      {path: 'students', component: Students, name: 'Dashboard.Students'},
      {path: 'subjects', component: Subjects, name: 'Dashboard.Subjects'},
    ]   
  },
]