import Vue from 'vue'
import App from './App.vue'

import {store} from './store/store'

import VueRouter from 'vue-router'
Vue.use(VueRouter)
import Routes from './routes'
export const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
  store: store
})
