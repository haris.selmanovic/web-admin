import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
state:{
  signInData:{
    email: 'test@test.com',
    password: 'test',
  },
  server: 'http://10.0.4.24:8081',
  students:[
    // {jmbg: '101010101010', name: 'Haris', lastName: 'Selmanovic', email: 'haris.selmanovic@untz.ba', password: 'test123'}, 
    // {jmbg: '202020202020', name: 'Adnan', lastName: 'Dizdarevic', email: 'adnan.dizdarevic@untz.ba', password: 'test123'},
    // {jmbg: '121212121212', name: 'Tarik', lastName: 'Dahic', email: 'tarik.dahic@untz.ba', password: 'test123'},
    // {jmbg: '303030303030', name: 'Tarik', lastName: 'Omercehajic', email: 'tarik.omercehajic@untz.ba', password: 'test123'},
    // {jmbg: '131313131313', name: 'Tarik', lastName: 'Fazlic', email: 'tarik.fazlic@untz.ba', password: 'test123'},
  ], 
  subjects:[
    // {id: '0', name: 'Matematika', token: 'MM'}, 
    // {id: '1', name: 'Fizika', token: 'FIZ'},
    // {id: '2', name: 'Elektronika', token: 'ELE'},
    // {id: '3', name: 'Osnove Programiranja', token: 'OP'},
    // {id: '4', name: 'Osnovi elektrotehnike', token: 'OE'},
  ]
}, 

getters:{
  signInData(state){
    return state.signInData; 
  },
  students(state){
    return state.students;
  }, 
  subjects(state){
    return state.subjects;
  },
  server (state) {
    return state.server;
  }
},

mutations:{
  setStudents(state, students){
    state.students = students; 
  },
  addStudent(state, student){
    var stud = state.students.find( elem => elem.jmbg === student.jmbg);
    if(stud === undefined) // student se ne nalazi u nizu 
      state.students.push(student);
  }, 
  removeStudent(state, student){
    var stud = state.students.find( elem => elem.jmbg === student.jmbg);
    if(stud !== undefined){
      let i = state.students.indexOf(stud);
      state.students.splice(i, 1);
    } 
  },
  setSubjects(state, subjects){
    state.subjects = subjects; 
  },
  addSubject(state, subject){
    var subj = state.subjects.find( elem => elem.id === subject.id);
    if(subj === undefined) // subject se ne nalazi u nizu 
      state.subjects.push(subject);
  }, 
  removeSubject(state, subject){
    var subj = state.subjects.find( elem => elem.id === subject.id);
    if(subj !== undefined){
      let i = state.subjects.indexOf(subj);
      state.subjects.splice(i, 1);
    } 
  }
}

})